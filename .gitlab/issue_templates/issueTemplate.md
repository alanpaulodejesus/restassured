### Descrição/Motivo:

Devido a criação de nova tela modal BSTQB, será necessário a criação de novos perfis de usuário ADMIN e FUNCIONARIO.

### Critérios Aceite:

- [ ] Usuário ADMIN deve possuir acesso gestão membros
- [ ] Usuário FUNCIONARIO não deve possuir acesso gestão membros
- [ ] Usuários de demais acessos não devem ter acesso a modal BSTQB


### Cenários de Testes:


- [ ] **Cenário usuário Admin**


&nbsp;
      Dado que acesso a aplicação https://bstqb.org.br/b9/  

&nbsp;
      E estou com o usuário de perfil de ADMIN  
      
&nbsp;
      Quando acesso o menu de cadastro  

&nbsp;
      Então aplicação apresenta modal de gestão para cadastro  
      
&nbsp;
- [ ]  **Cenário usuário NÃO Admin**

&nbsp;
      Dado que acesso a aplicação https://bstqb.org.br/b9/

&nbsp;
      E estou com o usuário de perfil FUNCIONARIO

&nbsp;
      Então aplicação NÃO apresenta modal de gestão para cadastro

&nbsp;
- [ ]  **Usuário Inválido**

&nbsp;
      Dado que possuo a aplicação https://bstqb.org.br/b9/

&nbsp;
      E usuário não é ADMIN e FUNCIONARIO

&nbsp;
      Então aplicação retorna que perfil é inválido

&nbsp;
- [ ]  **Erro 500 - Indisponível**

&nbsp;
      Dado que acesso a aplicação https://bstqb.org.br/b9/

&nbsp;
      E ela está indisponível

&nbsp;
      Então aplicação retorna erro 500

&nbsp;

### Artefato:

https://bstqb.org.br/b9/