package TestSuit;

import Secao1.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        JsonUsers.class,
        BodyValidar.class,
        JsonUsersAvancado.class,
        MacthersAssertiva.class,
        OlaMundo.class,
        XMLUsers.class
})
 public class SuitTest {

}
